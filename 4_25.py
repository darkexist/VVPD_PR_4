import argparse


def normalize_rectangle(x1, y1, x2, y2):
    return (
        min(x1, x2),
        min(y1, y2),
        max(x1, x2),
        max(y1, y2)
    )


def intersection(x1, y1, x2, y2, x3, y3, x4, y4) -> int:
    x1, y1, x2, y2 = normalize_rectangle(x1, y1, x2, y2)
    x3, y3, x4, y4 = normalize_rectangle(x3, y3, x4, y4)
    dx = max(0, min(x2, x4) - max(x1, x3))
    dy = max(0, min(y2, y4) - max(y1, y3))
    return dx * dy


def union(x1, y1, x2, y2, x3, y3, x4, y4) -> int:
    x1, y1, x2, y2 = normalize_rectangle(x1, y1, x2, y2)
    x3, y3, x4, y4 = normalize_rectangle(x3, y3, x4, y4)
    area1 = abs(x1 - x2) * abs(y1 - y2)
    area2 = abs(x3 - x4) * abs(y3 - y4)
    intersection_area = intersection(x1, y1, x2, y2, x3, y3, x4, y4)
    return area1 + area2 - intersection_area


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-r1", "--rectangle1", type=float, nargs=4, required=True)
    parser.add_argument("-r2", "--rectangle2", type=float, nargs=4, required=True)
    args = parser.parse_args()

    rect1 = args.rectangle1
    rect2 = args.rectangle2
    intersection_area = intersection(*(*rect1, *rect2))
    print("Площадь пересечения:", intersection_area)
    print("Площадь объединения:", union(*(*rect1, *rect2)))


if __name__ == "__main__":
    main()
